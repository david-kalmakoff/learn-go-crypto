scratch:
	go run app/scratch/main.go

node:
	go run app/services/node/main.go

test:
	go test ./...
