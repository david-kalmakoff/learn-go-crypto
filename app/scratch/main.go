package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"math/big"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
)

type Tx struct {
	FromID string `json:"from"`
	ToID   string `json:"to"`
	Value  uint64 `json:"value"`
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	privateKey, err := crypto.LoadECDSA("zblock/accounts/david.ecdsa")
	// privateKey, err := crypto.GenerateKey()
	if err != nil {
		return fmt.Errorf("unable to load private key for the node: %v", err)
	}

	// if err := crypto.SaveECDSA("zblock/accounts/david.ecdsa", privateKey); err != nil {
	// 	return fmt.Errorf("no save: %w", err)
	// }

	tx := Tx{
		FromID: "0x691a97a06103B208bc2Ef3fF16CF23E470bdc677",
		ToID:   "Aaron",
		Value:  100,
	}

	data, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	stamp := []byte(fmt.Sprintf("\x19Ardan Signed Message:\n%d", len(data)))
	v := crypto.Keccak256(stamp, data)

	sig, err := crypto.Sign(v, privateKey)
	if err != nil {
		return err
	}

	fmt.Println("SIG", string(hexutil.Encode(sig)))

	// =======================================
	// OVER THE WIRE

	publicKey, err := crypto.SigToPub(v, sig)
	if err != nil {
		return err
	}

	fmt.Println("PUB", crypto.PubkeyToAddress(*publicKey).String())

	// =======================================

	tx = Tx{
		FromID: "0x691a97a06103B208bc2Ef3fF16CF23E470bdc677",
		ToID:   "Frank",
		Value:  100,
	}

	data, err = json.Marshal(tx)
	if err != nil {
		return err
	}

	stamp = []byte(fmt.Sprintf("\x19Ardan Signed Message:\n%d", len(data)))
	v2 := crypto.Keccak256(stamp, data)

	sig2, err := crypto.Sign(v2, privateKey)
	if err != nil {
		return err
	}

	fmt.Println("SIG", string(hexutil.Encode(sig2)))

	// =======================================
	// OVER THE WIRE

	tx2 := Tx{
		FromID: "0x691a97a06103B208bc2Ef3fF16CF23E470bdc677",
		ToID:   "Frank",
		Value:  100,
	}

	data, err = json.Marshal(tx2)
	if err != nil {
		return err
	}

	stamp = []byte(fmt.Sprintf("\x19Ardan Signed Message:\n%d", len(data)))
	v2 = crypto.Keccak256(stamp, data)

	publicKey, err = crypto.SigToPub(v2, sig2)
	if err != nil {
		return err
	}

	fmt.Println("PUB", crypto.PubkeyToAddress(*publicKey).String())

	vv, r, s, err := ToVRSFromHexSignature(hexutil.Encode(sig2))
	if err != nil {
		return fmt.Errorf("unable to VRS: %w", err)
	}
	fmt.Println("V|R|S", vv, r, s)

	// =======================================
	// OVER THE WIRE

	fmt.Println("===== TX ======")
	davidTx, err := database.NewTx(1, 1, "0xe1244FFe8aEfe561E5d1DfC3bED7E49E0C3512ba", "0xa988b1866EaBF72B4c53b592c97aAD8e4b9bDCC0", 1000, 0, nil)
	if err != nil {
		return fmt.Errorf("unable to davidTx: %w", err)
	}

	signedTx, err := davidTx.Sign(privateKey)
	if err != nil {
		return fmt.Errorf("unable to sign: %w", err)
	}

	fmt.Println(signedTx)

	return nil
}

// ToVRSFromHexSignature converts a hex representation of the signature into
// its R, S and V parts.
func ToVRSFromHexSignature(sigStr string) (v, r, s *big.Int, err error) {
	sig, err := hex.DecodeString(sigStr[2:])
	if err != nil {
		return nil, nil, nil, err
	}

	r = big.NewInt(0).SetBytes(sig[:32])
	s = big.NewInt(0).SetBytes(sig[32:64])
	v = big.NewInt(0).SetBytes([]byte{sig[64]})

	return v, r, s, nil
}
