package main

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/ethereum/go-ethereum/crypto"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/genesis"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/state"
)

func main() {
	logger := slog.Default()

	if err := run(logger); err != nil {
		logger.Error("startup", "ERROR", err)
		os.Exit(1)
	}
}

func run(log *slog.Logger) error {
	// =========================================================================
	// Configuration

	var cfg struct {
		Web struct {
			ReadTimeout     time.Duration
			WriteTimeout    time.Duration
			IdleTimeout     time.Duration
			ShutdownTimeout time.Duration
			DebugHost       string
			PublicHost      string
			PrivateHost     string
		}
		State struct {
			Beneficiary    string
			DBPath         string
			SelectStrategy string
			OriginPeers    []string
			Consensus      string
		}
		NameService struct {
			Folder string
		}
	}

	cfg.Web.ReadTimeout = time.Duration(5 * time.Second)
	cfg.Web.WriteTimeout = time.Duration(10 * time.Second)
	cfg.Web.IdleTimeout = time.Duration(120 * time.Second)
	cfg.Web.ShutdownTimeout = time.Duration(20 * time.Second)
	cfg.Web.DebugHost = "0.0.0.0:7080"
	cfg.Web.PublicHost = "0.0.0.0:8080"
	cfg.Web.PrivateHost = "0.0.0.0:9080"

	cfg.State.Beneficiary = "miner1"
	cfg.State.DBPath = "zblock/miner1/"
	cfg.State.SelectStrategy = "Tip"
	cfg.State.OriginPeers = []string{"0.0.0.0:9080"}
	cfg.State.Consensus = "POW"

	cfg.NameService.Folder = "zblock/accounts/"

	// =========================================================================
	// App Starting

	log.Info("starting service")
	defer log.Info("shutdown complete")

	// Display the current configuration to the logs.
	log.Info("startup", "config", cfg)

	// ==========================================================================
	// Blockchain Support

	// Need to load the private key file for the configured beneficiary
	path := fmt.Sprintf("%s%s.ecdsa", cfg.NameService.Folder, cfg.State.Beneficiary)
	privateKey, err := crypto.LoadECDSA(path)
	if err != nil {
		return fmt.Errorf("unable to load private key for node: %w", err)
	}

	ev := func(v string, args ...any) {
		s := fmt.Sprintf(v, args...)
		log.Info(s, "traceid", "00000000-0000-0000-0000-000000000000")
	}

	// Load the genesis file
	genesis, err := genesis.Load()
	if err != nil {
		return err
	}

	// state represents the blockchain
	state, err := state.New(state.Config{
		BeneficiaryID:  database.PublicKeyToAccountID(privateKey.PublicKey),
		Genesis:        genesis,
		SelectStrategy: cfg.State.SelectStrategy,
		Evhandler:      ev,
	})
	defer state.Shutdown()

	return nil
}
