package mempool

import (
	"errors"
	"fmt"
	"math"
	"strings"
	"sync"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/mempool/selector"
)

// Mempool is a cache of transactions
type Mempool struct {
	mu       sync.RWMutex
	pool     map[string]database.BlockTx
	selectFn selector.Func
}

// New creates a new mempool
func New() (*Mempool, error) {
	return NewWithStrategy(selector.StrategyTip)
}

// NewWithStrategy creates a mempool with a specific sort strategy
func NewWithStrategy(strategy string) (*Mempool, error) {
	selectFn, err := selector.Retrieve(strategy)
	if err != nil {
		return nil, err
	}

	mp := Mempool{
		pool:     make(map[string]database.BlockTx),
		selectFn: selectFn,
	}

	return &mp, nil
}

// Count returns the number of transactions in the pool
func (mp *Mempool) Count() int {
	mp.mu.RLock()
	defer mp.mu.RUnlock()

	return len(mp.pool)
}

// Upsert adds and replaces a transaction in the mempool
func (mp *Mempool) Upsert(tx database.BlockTx) error {
	mp.mu.Lock()
	defer mp.mu.Unlock()

	// No limits are being put on the pool
	key, err := mapKey(tx)
	if err != nil {
		return err
	}

	// ETH requires a 10% bump in tip
	if etx, exists := mp.pool[key]; exists {
		if tx.Tip < uint64(math.Round(float64(etx.Tip)*1.10)) {
			return errors.New("replacing a transaction requires a 10% bump in the tip")
		}
	}

	mp.pool[key] = tx

	return nil
}

// Delete removes the transaction from the mempool
func (mp *Mempool) Delete(tx database.BlockTx) error {
	mp.mu.Lock()
	defer mp.mu.Unlock()

	key, err := mapKey(tx)
	if err != nil {
		return err
	}

	delete(mp.pool, key)

	return nil
}

// Truncate clears the pool
func (mp *Mempool) Truncate() {
	mp.mu.Lock()
	defer mp.mu.Unlock()

	mp.pool = make(map[string]database.BlockTx)
}

// PickBest uses the configured strategy to return txs
func (mp *Mempool) PickBest(howMany ...uint16) []database.BlockTx {
	mp.mu.Lock()
	defer mp.mu.Unlock()

	number := 0
	if len(howMany) > 0 {
		number = int(howMany[0])
	}

	m := make(map[database.AccountID][]database.BlockTx)
	mp.mu.Lock()
	{
		if number == 0 {
			number = len(mp.pool)
		}

		for key, tx := range mp.pool {
			account := accountFromMapKey(key)
			m[account] = append(m[account], tx)
		}
	}
	mp.mu.Unlock()

	return mp.selectFn(m, number)
}

// ============================================================================

// mapKey generates the map key
func mapKey(tx database.BlockTx) (string, error) {
	return fmt.Sprintf("%s:%d", tx.FromID, tx.Nonce), nil
}

// accountFromMapKey gets account from mapkey
func accountFromMapKey(key string) database.AccountID {
	return database.AccountID(strings.Split(key, ":")[0])
}
