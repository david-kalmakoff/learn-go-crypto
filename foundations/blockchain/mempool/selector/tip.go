package selector

import (
	"sort"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
)

// tipSelect returns txs with best tip respecting to the nonce
var tipSelect = func(m map[database.AccountID][]database.BlockTx, howMany int) []database.BlockTx {
	// Sort txs per account by nonce
	for key := range m {
		if len(m[key]) > 1 {
			sort.Sort(byNonce(m[key]))
		}
	}

	// Breaks txs into rounds
	var rows [][]database.BlockTx
	for {
		var row []database.BlockTx
		for key := range m {
			if len(m[key]) > 0 {
				row = append(row, m[key][0])
				m[key] = m[key][1:]
			}
		}
		if row == nil {
			break
		}
		rows = append(rows, row)
	}

	// Sort each row by tip
	final := []database.BlockTx{}
	for _, row := range rows {
		need := howMany - len(final)
		if len(row) > need {
			sort.Sort(byTip(row))
			final = append(final, row[:need]...)
			break
		}
		final = append(final, row...)
	}

	return final
}
