package database_test

import (
	"testing"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
)

func TestToAccountID(t *testing.T) {
	tests := []struct {
		name string
		hex  string
		err  string
		ret  database.AccountID
	}{
		{
			name: "handles valid id",
			hex:  "0xe1244FFe8aEfe561E5d1DfC3bED7E49E0C3512ba",
			err:  "",
			ret:  "0xe1244FFe8aEfe561E5d1DfC3bED7E49E0C3512ba",
		},
		{
			name: "handles empty id",
			hex:  "",
			err:  "invalid account format",
			ret:  "",
		},
		{
			name: "handles invalid id",
			hex:  "0xe1244FFe8aEfe561E5d1DfC3bED7E49E0C3512b",
			err:  "invalid account format",
			ret:  "",
		},
	}

	for _, tt := range tests {
		test := func(t *testing.T) {
			id, err := database.ToAccountID(tt.hex)
			if err != nil {
				if err.Error() != tt.err {
					t.Fatalf("\tF\tshould get proper err output; got: %v, want: %v", err, tt.err)
				}
			} else if tt.err != "" {
				t.Fatalf("\tF\tshould get proper err output; got: N/A, want: %v", tt.err)
			}
			t.Logf("\tP\tshould get proper err output")

			if id != tt.ret {
				t.Fatalf("\tF\tshould get proper id output; got: %v, want: %v", id, tt.ret)
			}
			t.Logf("\tP\tshould get proper id output")
		}

		t.Run(tt.name, test)
	}
}
