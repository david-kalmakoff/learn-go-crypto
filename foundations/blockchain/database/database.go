package database

import (
	"errors"
	"sync"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/genesis"
)

// Database manages data related to accounts
type Database struct {
	mu       sync.RWMutex
	genesis  genesis.Genesis
	accounts map[AccountID]Account
}

// New creates a new database
func New(genesis genesis.Genesis, evHandler func(v string, args ...any)) (*Database, error) {
	db := Database{
		genesis:  genesis,
		accounts: make(map[AccountID]Account),
	}

	for accountStr, balance := range genesis.Balances {
		accountID, err := ToAccountID(accountStr)
		if err != nil {
			return nil, err
		}
		db.accounts[accountID] = newAccount(accountID, balance)

		evHandler("Account: %s, Balance: %d", accountID, balance)
	}

	return &db, nil
}

// Remove delete account from DB
func (db *Database) Remove(accountID AccountID) {
	db.mu.Lock()
	defer db.mu.Unlock()

	delete(db.accounts, accountID)
}

// Query retrieves an account from DB
func (db *Database) Query(accountID AccountID) (Account, error) {
	db.mu.Lock()
	defer db.mu.Unlock()

	account, exists := db.accounts[accountID]
	if !exists {
		return Account{}, errors.New("account does not exist")
	}

	return account, nil
}

// Copy makes a copy of the DB
func (db *Database) Copy() map[AccountID]Account {
	db.mu.Lock()
	defer db.mu.Unlock()

	accounts := make(map[AccountID]Account)
	for accountID, account := range db.accounts {
		accounts[accountID] = account
	}

	return accounts
}
