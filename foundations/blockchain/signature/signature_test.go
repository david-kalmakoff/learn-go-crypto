package signature_test

import (
	"testing"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/helpers"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/signature"
)

func TestSign(t *testing.T) {
	key, err := helpers.GeneratePrivateKey()
	if err != nil {
		t.Fatalf("\tF\tshould generate key: %v", err)
	}
	addr, err := helpers.GetAddressFromPrivateKey(key)
	if err != nil {
		t.Fatalf("\tF\tshould get address: %v", err)
	}

	v, r, s, err := signature.Sign("something", key)
	if err != nil {
		t.Fatalf("\tF\tshould sign: %v", err)
	}

	if err := signature.VerifySignature(v, r, s); err != nil {
		t.Fatalf("\tF\tshould verify: %v", err)
	}
	t.Logf("\tP\tshould verify signature")

	from, err := signature.FromAddress("something", v, r, s)
	if err != nil {
		t.Fatalf("\tF\tshould get from address: %v", err)
	}
	if from != string(addr) {
		t.Fatalf("\tF\tshould get matching addresses: got %s, want %s", from, addr)
	}
	t.Logf("\tP\tshould validate address")
}
