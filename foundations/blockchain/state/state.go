package state

import (
	"sync"

	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/database"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/genesis"
	"gitlab.com/david-kalmakoff/learn-go-crypto/foundations/blockchain/mempool"
)

// EventHandler is a function that occurs when processing blocks
type EventHandler func(v string, args ...any)

// ============================================================================

// Config is required to start the node
type Config struct {
	BeneficiaryID  database.AccountID
	Genesis        genesis.Genesis
	SelectStrategy string
	Evhandler      EventHandler
}

// State manages the DB
type State struct {
	mu sync.RWMutex

	beneficiaryID database.AccountID
	evHandler     EventHandler

	genesis genesis.Genesis
	mempool *mempool.Mempool
	db      *database.Database
}

// New creates a new blockchain for management
func New(cfg Config) (*State, error) {
	// Build sage EV for use
	ev := func(v string, args ...any) {
		if cfg.Evhandler != nil {
			cfg.Evhandler(v, args...)
		}
	}

	// Access the storage for the blockchain
	db, err := database.New(cfg.Genesis, ev)
	if err != nil {
		return nil, err
	}

	// Construct a mempool with the specified sort strategy
	mempool, err := mempool.NewWithStrategy(cfg.SelectStrategy)
	if err != nil {
		return nil, err
	}

	// Create the state
	state := State{
		beneficiaryID: cfg.BeneficiaryID,
		evHandler:     ev,

		genesis: cfg.Genesis,
		mempool: mempool,
		db:      db,
	}

	return &state, nil
}

// Shutdown cleanly brings node down
func (s *State) Shutdown() error {
	s.evHandler("state: shutdown: started")
	defer s.evHandler("state: shutdown: completed")

	return nil
}

// MempoolLength returns the current length of the mempool
func (s *State) MempoolLength() int {
	return s.mempool.Count()
}

// Mempool returns a copy of the mempool
func (s *State) Mempool() []database.BlockTx {
	return s.mempool.PickBest()
}

// UpsertMempool adds a new transaction to the mempool
func (s *State) UpsertMempool(tx database.BlockTx) error {
	return s.mempool.Upsert(tx)
}

// Accounts returns a copy of the database accounts
func (s *State) Accounts() map[database.AccountID]database.Account {
	return s.db.Copy()
}
